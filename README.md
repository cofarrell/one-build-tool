# One tool to build them all

![One Ring](http://jlheuer.files.wordpress.com/2011/03/one-ring.jpg)

> Build tools should use a build cache, emphasis on the cache, for propagating results from one component to another. A cache is an abstraction that allows computing a function more quickly based on partial results computed in the past. The function, in this case, is for turning source code into a binary.
> A cache does nothing except speed things up. You could remove a cache entirely and the surrounding system would work the same, just more slowly. A cache has no side effects, either. No matter what you've done with a cache in the past, a given query to the cache will give back the same value to the same query in the future.

The above quote was taken from Lex Spoon's amazing
[Recursive Maven Considered Harmful](http://blog.lexspoon.org/2012/12/recursive-maven-considered-harmful.html).
I stumbled across Lex's post in the equally good 
[In Quest of the Ultimate Build Tool](http://blog.ltgt.net/in-quest-of-the-ultimate-build-tool/).

Over the past couple of months I've begun my own quest, to seek out new build
tools and to boldly shave a yak no developer has shaved before.

- [Part 1 - Everyone is cheating](01-cheating.md)
- [Part 2 - Ultimate Build Tool](02-ultimate-build-tool.md)
- [Part 3 - Nix](03-nix.md)

Beware: This series doesn't contain any answers. I wish it did.
